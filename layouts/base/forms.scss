/**
 * forms.scss
 */

form {
    font-size: #{$form-font-size}px;
    color: $text-color;
}

fieldset {
    background-color: tint($ci-color-dark, 90%);
    border: none;
    box-sizing: border-box;
    padding: 0em 1em 1em 1em;
}

legend {
    background-color: tint($ci-color-dark, 90%);
    padding: 0.5em 1.15em;
    margin-left: -1em;
    font-weight: bold;
}

label {
    display: block;
    width: 100%;
    font-size: #{$form-font-size * 0.9}px;
    margin: 15px 0 5px 2px;

    &.checkbox,
    &.radio {
        cursor: pointer;
        width: auto;
        @include inline-block();
        margin: 0 20px 0 2px;

        span {
            border: solid 1px $form-border-color;
            font-size: 14px;
            @include inline-block();
            width: 20px;
            height: 20px;
            text-align: center;
            border-radius: 20px;
            line-height: 16px;
            margin-right: 8px;
            color: $ci-color-light;
            font-family: "EmojiSymbols";
            &::before {content: '\00A0';}
        }

    }

}

input[type=checkbox]:checked + label > span {
    &::before {content: '\2714';}
    border-color: $ci-color-light;
    color: $ci-color-light;
    background-color: $body-background-color;
}

input[type=radio]:checked + label > span {
    &::before {content: '\25CF';}
    border-color: $ci-color-light;
    color: $ci-color-light;
    background-color: $body-background-color;
}

input[type=text],
input[type=email],
input[type=file],
textarea {
    display: block;
    width: 100%;
    border: solid 1px $form-border-color;
    padding: $form-padding;
    box-sizing: border-box;
    font-size: #{$form-font-size}px;
}

input[type=checkbox],
input[type=radio] {
    display: none;
}

select {
    border: solid 1px $form-border-color;
    font-size: #{$base-font-size * 0.9}px;
    padding: 1em;
    height: 30px;
}

input[type=submit],
input[type=reset],
button {
    cursor: pointer;
    border: solid 1px $form-border-color;
    font-size: #{$base-font-size * 0.9}px;
    padding: 0.75em 1.5em;
    background-color: $ci-color-light;
    color: white;
    border: none;
    transition: background-color 0.25s;
    margin: 20px 0 0 0;

    &.alert {
        background-color: $alert-color;
        &:hover {
            background-color: tint($alert-color, 20);
        }
    }

    &.success {
        background-color: $success-color;
        &:hover {
            background-color: tint($success-color, 20);
        }
    }

    &:hover {
        background-color: tint($ci-color-light, 20);
    }

}

.errorlist {
    background-color: tint($alert-color, 80);
    margin: 10px 0;
    list-style: none;
    color: $alert-color;
    font-size: #{$base-font-size * 0.9}px;
    border-radius: 5px;
    li {
        padding: 0.75em 1em;
        &:before {
            content: "";
        }
    }
}

.success {
    background-color: tint($success-color, 80);
    color: $success-color;
    margin: 10px 0;
    padding: 0.75em 1em;
    font-size: #{$base-font-size * 0.9}px;
    border-radius: 5px;

    p {
        color: $success-color;
        margin: 0;
    }

}
