#!/bin/bash

# Aufruf:
# ./watch.sh [projekt]
# ./watch.sh landfried-stiftung
sass -r sass-globbing --watch $1.scss:../css/$1.css $1-addons.scss:../css/$1-addons.css
