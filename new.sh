#!/bin/bash

# Aufruf:
# ./new.sh [projekt]
# ./new.sh landfried-stiftung

mkdir layouts/$1
touch layouts/$1/page.scss
touch layouts/$1/typo.scss
cp base.scss $1.scss
touch _$1-config.scss
touch $1-addons.scss
